import React, { useState, useContext } from 'react';

const CalendarDay = ({ date, classes, setIsModalOpen }) => {
  return (
    <div
      className={`calendar__grid-day ${classes}`}
      data-unix={date.unix()}
      onClick={() => setIsModalOpen(true)}>
      <span className="dateNumber">{date.get('date')}</span>
      <span className="dot">O</span>
      <span className="shift">VK</span>
    </div>
  );
};

export default CalendarDay;
