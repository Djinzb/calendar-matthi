import React, { useState } from 'react';
import dayjs from 'dayjs';
import styled from 'styled-components';
import CalendarDay from './CalendarDay';
import Modal from '../modal';

import {
  getDaysInMonth,
  getCurrentMonth,
  getCurrentYear,
  getFirstDayOfMonth,
  prevMonth,
  nextMonth,
  constructDate,
  isCurrentDay,
  zeroFill,
} from './CalendarPure';

const CalendarStyled = styled.div`
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

const Heading = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0.7rem 0.5rem;
  .month {
    font-size: 1.5rem;
    letter-spacing: 2px;
  }
  .button {
    appearance: none;
    width: 1rem;
    height: 1rem;
    border-radius: 50%;
    padding: 1rem;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;

    span {
      position: absolute;
      font-size: 0.6rem;
    }
  }
`;

const CalHead = styled.div`
  display: grid;
  grid-template-columns: repeat(7, 4.2rem);
  grid-template-rows: 2.6rem;
  grid-gap: 0.625rem;

  .calendar__grid-weekday {
    border-radius: 0.6rem;
    background-color: #3f4854;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const CalBody = styled.div`
  display: grid;
  grid-template-columns: repeat(7, 4.2rem);
  grid-template-rows: repeat(6, 4.8rem);
  grid-gap: 0.625rem;
  margin-top: 0.625rem;
  .calendar__grid-day {
    border-radius: 0.6rem;
    background-color: #f2f2f2;
    color: #3b5350;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 0.4fr 1.6fr;
    box-shadow: 0 2px 2px 0 rgba(91, 102, 105, 0.15),
      0 3px 3px 0 rgba(91, 102, 105, 0.05);
    cursor: pointer;
    &.notInMonth {
      opacity: 0.7;
    }
    .dateNumber {
      padding: 0.3rem;
    }
    .dot {
      padding: 0.3rem;
      text-align: right;
    }
    .shift {
      grid-column: span 2;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 0.6rem;
      background-color: #4b7997;
      margin: 0.3rem;
    }
  }
`;

const Calendar = props => {
  const [dateObject, setDateObject] = useState(dayjs());
  const [isModalOpen, setIsModalOpen] = useState(false);

  const renderWeekdaysHeader = () => {
    const weekdayshort = ['MA', 'DI', 'WO', 'DO', 'VR', 'ZA', 'ZO'];
    const weekdaysHeader = [];
    weekdayshort.forEach((day, i) => {
      weekdaysHeader.push(
        <span className="calendar__grid-weekday" key={'weekdays' + i}>
          {day}
        </span>
      );
    });
    return weekdaysHeader;
  };

  const renderDay = (key, date, classes) => {
    return (
      <CalendarDay
        key={key}
        date={date}
        classes={classes}
        setIsModalOpen={setIsModalOpen}
      />
    );
  };

  const renderDays = () => {
    const amountOfDaysPrevMonth = dateObject.subtract(1, 'month').daysInMonth();
    const days = [];

    const daysPrevMonth = getFirstDayOfMonth(dateObject);
    const daysInMonth = getDaysInMonth(dateObject);
    const daysNextMonth = 7 - ((daysPrevMonth + daysInMonth) % 7);
    const totalDays = daysPrevMonth + daysInMonth + daysNextMonth;

    let day = null;
    let month = null;
    const year = getCurrentYear(dateObject);
    let className = '';

    for (let i = 1; i <= totalDays; i++) {
      if (i <= getFirstDayOfMonth(dateObject)) {
        // DAYS IN MONTH BEFORE
        day = amountOfDaysPrevMonth - getFirstDayOfMonth(dateObject) + i;
        month = dateObject.subtract(1, 'month').format('MM');
        className = 'notInMonth notInMonth--prev';
      } else if (i <= daysPrevMonth + daysInMonth) {
        // DAYS IN CURRENT MONTH
        day = i - getFirstDayOfMonth(dateObject);
        month = dateObject.format('MM');
        const activeClass = isCurrentDay(dateObject, day) ? 'active' : '';
        className = `inMonth ${activeClass}`;
      } else {
        // DAYS IN MONTH AFTER
        day = i - (daysPrevMonth + daysInMonth);
        month = dateObject.add(1, 'month').format('MM');
        className = `notInMonth notInMonth--next`;
      }

      const date = constructDate(zeroFill(day), month, year);
      days.push(renderDay(i, date, className));
    }
    return days;
  };

  const filterPlansByDate = date => {
    // const plans = props.plans;
    // return plans.map((plan, i) => {
    //   if (dayjs(plan.publicationDate).isSame(date, 'date')) {
    //     return <CalendarEvent plan={plan} key={`plan` + i} />;
    //   }
    //   return null;
    // });
  };

  return (
    <>
      <CalendarStyled>
        <Heading>
          <button
            onClick={e => setDateObject(prevMonth(dateObject))}
            className="button buttonLeft">
            <span>{'<'}</span>
          </button>
          <div className="month">
            {getCurrentMonth(dateObject) + ' ' + getCurrentYear(dateObject)}
          </div>
          <button
            onClick={e => setDateObject(nextMonth(dateObject))}
            className="button buttonRight">
            <span>{'>'}</span>
          </button>
        </Heading>
        <div>
          <CalHead>{renderWeekdaysHeader()}</CalHead>
          <CalBody>{renderDays()}</CalBody>
        </div>
      </CalendarStyled>
      {isModalOpen && (
        <Modal setIsModalOpen={setIsModalOpen}>
          <AddCalendarItem />
        </Modal>
      )}
    </>
  );
};

const AddCalendarItem = ({ date }) => {
  const shifts = ['Vroegen', 'Laten', 'Nacht'];
  const [selectedShift, setSelectedShift] = useState(shifts[0]);
  const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();

  const AddShiftForm = styled.form`
    width: 100%;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
      'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    .dates {
      display: flex;
      justify-content: space-between;
      .dateInput {
        width: 50%;
      }
    }
    .submitButton {
      display: block;
      position: relative;
      padding: 1rem 3.5rem 1rem 0.75rem;
      width: 100%;
      font-size: 1rem;
      border: 1px solid #8292a2;
      border-radius: 0.25rem;
      appearance: none;
      margin-top: 2rem;
      background-color: #4b7997;
    }
    select {
      display: block;
      position: relative;
      padding: 1rem 3.5rem 1rem 0.75rem;
      width: 100%;
      font-size: 1rem;
      border: 1px solid #8292a2;
      border-radius: 0.25rem;
      appearance: none;
      margin-bottom: 1rem;
    }

    input[type='date'] {
      display: block;
      position: relative;
      padding: 1rem 3.5rem 1rem 0.75rem;
      width: 75%;
      font-size: 1rem;
      border: 1px solid #8292a2;
      border-radius: 0.25rem;
      background: white
        url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='22' viewBox='0 0 20 22'%3E%3Cg fill='none' fill-rule='evenodd' stroke='%23688EBB' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' transform='translate(1 1)'%3E%3Crect width='18' height='18' y='2' rx='2'/%3E%3Cpath d='M13 0L13 4M5 0L5 4M0 8L18 8'/%3E%3C/g%3E%3C/svg%3E")
        right 1rem center no-repeat;
      cursor: pointer;
    }
    input[type='date']:focus {
      outline: none;
      border-color: #3acfff;
      box-shadow: 0 0 0 0.25rem rgba(0, 120, 250, 0.1);
    }

    ::-webkit-datetime-edit {
    }
    ::-webkit-datetime-edit-fields-wrapper {
    }
    ::-webkit-datetime-edit-month-field:hover,
    ::-webkit-datetime-edit-day-field:hover,
    ::-webkit-datetime-edit-year-field:hover {
      background: rgba(0, 120, 250, 0.1);
    }
    ::-webkit-datetime-edit-text {
      opacity: 0;
    }
    ::-webkit-clear-button,
    ::-webkit-inner-spin-button {
      display: none;
    }
    ::-webkit-calendar-picker-indicator {
      position: absolute;
      width: 2.5rem;
      height: 100%;
      top: 0;
      right: 0;
      bottom: 0;

      opacity: 0;
      cursor: pointer;

      color: rgba(0, 120, 250, 1);
      background: rgba(0, 120, 250, 1);
    }

    input[type='date']:hover::-webkit-calendar-picker-indicator {
      opacity: 0.05;
    }
    input[type='date']:hover::-webkit-calendar-picker-indicator:hover {
      opacity: 0.15;
    }
  `;

  const addShiftToCalendar = e => {
    e.preventDefault();
    const shift = {
      user: 'Matthias',
      name: selectedShift,
      from: fromDate,
      to: toDate,
    };
    alert(
      `${shift.user} heeft shift ${shift.name} toegevoegd van ${shift.from} tot ${shift.to}`
    );
  };
  return (
    <AddShiftForm onSubmit={e => addShiftToCalendar(e)}>
      <select
        onChange={e => setSelectedShift(e.target.value)}
        defaultValue={selectedShift}>
        {shifts.map((shift, i) => (
          <option key={`shiftOption${i}`} defaultValue={shift} id={`shift${i}`}>
            {shift}
          </option>
        ))}
      </select>
      <div className="dates">
        <span className="dateInput">
          <label>Van:</label>
          <input
            key={'fromDate'}
            type="date"
            defaultValue={fromDate}
            onChange={e => setFromDate(e.target.value)}></input>
        </span>
        <span className="dateInput">
          <label>Tot:</label>
          <input
            key={'toDate'}
            type="date"
            defaultValue={toDate}
            onChange={e => setToDate(e.target.value)}></input>
        </span>
      </div>
      <input className="submitButton" type="submit" value="Voeg toe" />
    </AddShiftForm>
  );
};

export default Calendar;
