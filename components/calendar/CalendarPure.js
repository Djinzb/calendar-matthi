import dayjs from 'dayjs';

const shiftSundayToBack = day => {
  // in startOf of dayjs is sunday set to 0
  const week = [1, 2, 3, 4, 5, 6, 0];
  return week.indexOf(day);
};

export const getDaysInMonth = date => {
  return date.daysInMonth();
};

export const getCurrentDay = date => {
  return date.get('date');
};

export const getCurrentMonth = date => {
  return date.format('MMMM');
};

export const getCurrentYear = date => {
  return date.format('YYYY');
};

export const getFirstDayOfMonth = date => {
  let firstDay = dayjs(date)
    .startOf('month')
    .get('d');
  return shiftSundayToBack(firstDay);
};

export const prevMonth = date => {
  return dayjs(date.subtract(1, 'month'));
};

export const nextMonth = date => {
  return dayjs(date.add(1, 'month'));
};

export const constructDate = (day, month, year) => {
  return dayjs(`${year}-${month}-${day}`);
};

export const isCurrentDay = (dateObject, day) =>
  day === getCurrentDay(dateObject) && dayjs().month() === dateObject.month();

export const zeroFill = i => {
  return (i < 10 ? '0' : '') + i;
};
