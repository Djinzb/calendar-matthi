import styled from 'styled-components';

const ModalStyled = styled.div`
  position: fixed;
  z-index: 9999999;
  top: 0;
  width: 100vw;
  height: 100vh;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;

  .backdrop {
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.6);
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .container {
    min-width: 50vw;
    background-color: #fff;
    box-shadow: 0 0.6875rem 1.3125rem 0 rgba(0, 0, 0, 0.6);
    border-radius: 0.1875rem;
  }

  .header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 0.625rem;
    height: 3rem;
    border-bottom: 0.0625rem solid grey;

    h3 {
      letter-spacing: 1px;
      font-weight: 200;
    }
  }

  .content {
    padding: 0.8625rem;
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: space-between;
    .form-group {
      max-width: 21.125rem;

      .input {
        margin-bottom: 0.5rem;
      }

      .label--error {
        line-height: 1.4;
      }
    }
  }

  .close {
    display: flex;
    text-decoration: none;
  }
`;

const Modal = ({ setIsModalOpen, children }) => {
  const closeModal = e => {
    e.preventDefault();
    setIsModalOpen(false);
  };

  return (
    <ModalStyled>
      <div className="backdrop">
        <div className="container">
          <header className="header">
            <h3>Shift toevoegen</h3>
            <a href="#" onClick={closeModal} className="close">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24">
                <g fill="#000" fillRule="nonzero">
                  <path d="M7 19a1 1 0 0 1-.71-1.71l10-10a1.004 1.004 0 0 1 1.42 1.42l-10 10A1 1 0 0 1 7 19z" />
                  <path d="M17 19a1 1 0 0 1-.71-.29l-10-10a1.004 1.004 0 0 1 1.42-1.42l10 10A1 1 0 0 1 17 19z" />
                </g>
              </svg>
            </a>
          </header>

          <div className="content">{children}</div>
        </div>
      </div>
    </ModalStyled>
  );
};

export default Modal;
