import React from 'react';
import CalendarComponent from '../../components/calendar/Calendar';
import styled from 'styled-components';

const CalendarPage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;
const Calendar = () => {
  return (
    <CalendarPage>
      <CalendarComponent />
    </CalendarPage>
  );
};

export default Calendar;
